FROM openjdk:11
COPY build/libs/acc-0.0.1-SNAPSHOT.jar /app/
EXPOSE 8080
ENTRYPOINT ["java", "-jar","/app/acc-0.0.1-SNAPSHOT.jar"]