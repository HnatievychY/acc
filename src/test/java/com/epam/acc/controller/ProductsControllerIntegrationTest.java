package com.epam.acc.controller;

import com.epam.acc.dto.request.ProductsRequest;
import com.epam.acc.exeption.CategoriesNotFoundException;
import com.epam.acc.exeption.CurrencyNotSupportException;
import com.epam.acc.exeption.ProductsNotFoundException;
import com.epam.acc.model.Categories;
import com.epam.acc.model.LatestRate;
import com.epam.acc.model.Products;
import com.epam.acc.repository.CategoriesRepository;
import com.epam.acc.repository.ProductsRepository;
import com.epam.acc.service.FixerIoService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.security.test.context.support.WithMockUser;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;

import java.math.BigDecimal;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Optional;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;


@AutoConfigureMockMvc(addFilters = false)
@SpringBootTest
@ActiveProfiles(profiles = "test")
class ProductsControllerIntegrationTest {

    @Autowired
    private MockMvc mockMvc;
    @MockBean
    private ProductsRepository productsRepositoryMock;
    @MockBean
    private CategoriesRepository categoriesRepositoryMock;
    @MockBean
    private FixerIoService fixerIoServiceMock;
    @Autowired
    ObjectMapper objectMapper;


    private Products productsTest;
    private Products productsTestSecond;
    private List<Products> productsListTest;
    private LatestRate latestRate;
    private ProductsRequest productsRequestTestWithCurrencyEUR;
    private ProductsRequest productsRequestTestWithCurrencyUSD;
    private ProductsRequest productsRequestTestWithEmptyName;
    private Categories categoriesTest;

    private final String CURRENCY_BASE = "EUR";
    private final String CURRENCY_USD = "USD";
    private final String CURRENCY_BAD = "UE";
    private final String CURRENCY_NOT_SUPPORT = "PPP";

    private static final Long PRODUCT_ID_NOT_FOUND = 1L;
    private static final Long PRODUCT_ID_BAD_VALIDATION = -1L;

    private static final Long PRODUCT_ID = 98L;
    private static final String PRODUCT_NAME = "testProductName";
    private static final BigDecimal PRODUCT_PRICE = new BigDecimal("10.12");
    private static final BigDecimal PRODUCT_PRICE_IN_USD = new BigDecimal("12.14");

    private static final Long PRODUCT_ID_SECOND = 97L;
    private static final String PRODUCT_NAME_SECOND = "testProductName";
    private static final BigDecimal PRODUCT_PRICE_SECOND = new BigDecimal("20.12");

    private static final Long CATEGORY_ID = 147L;
    private static final String CATEGORY_NAME = "categoryName";

    private Map<String, BigDecimal> rateMap;


    @BeforeEach
    void setUp() {
        categoriesTest = new Categories();
        categoriesTest.setCategoryId(CATEGORY_ID);
        categoriesTest.setName(CATEGORY_NAME);

        productsTest = new Products();
        productsTest.setProductId(PRODUCT_ID);
        productsTest.setName(PRODUCT_NAME);
        productsTest.setPrice(PRODUCT_PRICE);
        productsTest.setCategory(categoriesTest);

        productsRequestTestWithCurrencyEUR = new ProductsRequest();
        productsRequestTestWithCurrencyEUR.setName(PRODUCT_NAME_SECOND);
        productsRequestTestWithCurrencyEUR.setPrice(PRODUCT_PRICE_SECOND);
        productsRequestTestWithCurrencyEUR.setCurrency(CURRENCY_BASE);
        productsRequestTestWithCurrencyEUR.setCategoryId(CATEGORY_ID);

        productsRequestTestWithEmptyName = new ProductsRequest();
        productsRequestTestWithEmptyName.setPrice(PRODUCT_PRICE);
        productsRequestTestWithEmptyName.setCurrency(CURRENCY_BASE);
        productsRequestTestWithEmptyName.setCategoryId(CATEGORY_ID);

        productsRequestTestWithCurrencyUSD = new ProductsRequest();
        productsRequestTestWithCurrencyUSD.setName(PRODUCT_NAME);
        productsRequestTestWithCurrencyUSD.setPrice(PRODUCT_PRICE);
        productsRequestTestWithCurrencyUSD.setCurrency(CURRENCY_USD);
        productsRequestTestWithCurrencyUSD.setCategoryId(CATEGORY_ID);

        productsTestSecond = new Products();
        productsTestSecond.setProductId(PRODUCT_ID_SECOND);
        productsTestSecond.setName(PRODUCT_NAME_SECOND);
        productsTestSecond.setPrice(PRODUCT_PRICE_SECOND);
        productsTestSecond.setCategory(categoriesTest);

        productsListTest = List.of(productsTest, productsTestSecond);

        rateMap = Collections.singletonMap("USD", new BigDecimal(1.2));
        latestRate = new LatestRate();
        latestRate.setBase(CURRENCY_BASE);
        latestRate.setRates(rateMap);
    }

    @Test
    void getProductByIdSuccess() throws Exception {
        when(this.productsRepositoryMock.findById(anyLong()))
                .thenReturn(Optional.ofNullable(productsTest));

        mockMvc.perform(
                get("/products/{id}", PRODUCT_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.name").value(PRODUCT_NAME))
                .andExpect(jsonPath("$.price").value(PRODUCT_PRICE))
                .andExpect(jsonPath("$.currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.category.categoryId").value(CATEGORY_ID));
    }

    @Test
    void getProductByIdNotFoundException() throws Exception {
        // given
        when(this.productsRepositoryMock.findById(anyLong()))
                .thenThrow(ProductsNotFoundException.class);
        // when
        mockMvc.perform(
                get("/products/{id}", PRODUCT_ID_NOT_FOUND)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void getProductByIdValidationException() throws Exception {
        // when
        mockMvc.perform(
                get("/products/{id}", PRODUCT_ID_BAD_VALIDATION)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());

    }

    @Test
    void getAllProducts() throws Exception {
        //given
        when(this.productsRepositoryMock.findAll())
                .thenReturn(this.productsListTest);
        // when
        mockMvc.perform(
                get("/products")
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.[0].name").value(PRODUCT_NAME))
                .andExpect(jsonPath("$.[0].price").value(PRODUCT_PRICE))
                .andExpect(jsonPath("$.[0].currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.[0].category.categoryId").value(CATEGORY_ID))
                .andExpect(jsonPath("$.[1].productId").value(PRODUCT_ID_SECOND))
                .andExpect(jsonPath("$.[1].name").value(PRODUCT_NAME_SECOND))
                .andExpect(jsonPath("$.[1].price").value(PRODUCT_PRICE_SECOND))
                .andExpect(jsonPath("$.[1].currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.[1].category.categoryId").value(CATEGORY_ID));
    }

    @Test
    void getProductByIdOtherCurrencyUSDSuccess() throws Exception {
        //given
        when(this.productsRepositoryMock.findById(anyLong()))
                .thenReturn(Optional.ofNullable(this.productsTest));
        when(this.fixerIoServiceMock.latestRate(anyString()))
                .thenReturn(latestRate);
        // when
        mockMvc.perform(
                get("/products/{id}/{currencySymbol}", PRODUCT_ID, CURRENCY_USD)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.name").value(PRODUCT_NAME))
                .andExpect(jsonPath("$.price").value(PRODUCT_PRICE_IN_USD))
                .andExpect(jsonPath("$.currency").value(CURRENCY_USD))
                .andExpect(jsonPath("$.category.categoryId").value(CATEGORY_ID));
    }

    @Test
    void getProductByIdOtherCurrencyBadCurrencySymbol() throws Exception {
        // when
        mockMvc.perform(
                get("/products/{id}/{currencySymbol}", PRODUCT_ID, CURRENCY_BAD)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void getProductByIdOtherCurrencyCorrectCurrencySymbolButNoSupport() throws Exception {
        //given
        when(this.productsRepositoryMock.findById(anyLong()))
                .thenReturn(Optional.ofNullable(this.productsTest));
        when(this.fixerIoServiceMock.latestRate(anyString()))
                .thenThrow(CurrencyNotSupportException.class);
        // when
        mockMvc.perform(
                get("/products/{id}/{currencySymbol}", PRODUCT_ID, CURRENCY_NOT_SUPPORT)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isBadRequest());
    }

    @Test
    void findProductByCategoryNotFound() throws Exception {
        //given
        when(this.categoriesRepositoryMock.findById(anyLong()))
                .thenThrow(CategoriesNotFoundException.class);
        // when
        mockMvc.perform(
                get("/products/category/{id}", CATEGORY_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isNotFound());
    }

    @Test
    void findProductByCategorySuccess() throws Exception {
        //given
        when(this.categoriesRepositoryMock.findById(CATEGORY_ID))
                .thenReturn(Optional.ofNullable(categoriesTest));
        when(this.productsRepositoryMock.findByCategory(CATEGORY_ID))
                .thenReturn(productsListTest);
        // when
        mockMvc.perform(
                get("/products/category/{id}", CATEGORY_ID)
                        .accept(MediaType.APPLICATION_JSON))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.[0].productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.[0].name").value(PRODUCT_NAME))
                .andExpect(jsonPath("$.[0].price").value(PRODUCT_PRICE))
                .andExpect(jsonPath("$.[0].currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.[0].category.categoryId").value(CATEGORY_ID))
                .andExpect(jsonPath("$.[1].productId").value(PRODUCT_ID_SECOND))
                .andExpect(jsonPath("$.[1].name").value(PRODUCT_NAME_SECOND))
                .andExpect(jsonPath("$.[1].price").value(PRODUCT_PRICE_SECOND))
                .andExpect(jsonPath("$.[1].currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.[1].category.categoryId").value(CATEGORY_ID));
    }

    @Test
    void createProductSuccessWithBaseCurrency() throws Exception {
        //given
        when(this.categoriesRepositoryMock.findById(CATEGORY_ID))
                .thenReturn(Optional.ofNullable(categoriesTest));
        when(this.productsRepositoryMock.save(any(Products.class)))
                .thenReturn(productsTest);

        // when
        mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productsRequestTestWithCurrencyEUR)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.name").value(PRODUCT_NAME))
                .andExpect(jsonPath("$.price").value(PRODUCT_PRICE))
                .andExpect(jsonPath("$.currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.category.categoryId").value(CATEGORY_ID));
    }

    @Test
    void createProductValidationException() throws Exception {
        // when
        mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productsRequestTestWithEmptyName)))
                .andExpect(status().isBadRequest());
    }

    @Test
    void createProductSuccessWithUsdCurrecy() throws Exception {
        //given
        when(this.categoriesRepositoryMock.findById(CATEGORY_ID))
                .thenReturn(Optional.ofNullable(categoriesTest));
        when(this.productsRepositoryMock.save(any(Products.class)))
                .thenReturn(productsTest);
        when(this.fixerIoServiceMock.latestRate(anyString()))
                .thenReturn(latestRate);

        // when
        mockMvc.perform(post("/products")
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(productsRequestTestWithCurrencyUSD)))
                .andExpect(status().isCreated())
                .andExpect(jsonPath("$.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.name").value(PRODUCT_NAME))
                .andExpect(jsonPath("$.price").value(PRODUCT_PRICE))
                .andExpect(jsonPath("$.currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.category.categoryId").value(CATEGORY_ID));
    }

    @Test
    void update() throws Exception {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID))
                .thenReturn(Optional.ofNullable(productsTest));
        when(this.productsRepositoryMock.save(any(Products.class)))
                .thenReturn(productsTest);
        when(this.categoriesRepositoryMock.findById(productsRequestTestWithCurrencyEUR.getCategoryId()))
                .thenReturn(Optional.ofNullable(categoriesTest));

        // when
        mockMvc.perform(
                put("/products/{id}", PRODUCT_ID)
                        .contentType(MediaType.APPLICATION_JSON_VALUE)
                        .content(this.objectMapper.writeValueAsString(productsRequestTestWithCurrencyEUR))
                        .accept(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.productId").value(PRODUCT_ID))
                .andExpect(jsonPath("$.name").value(PRODUCT_NAME_SECOND))
                .andExpect(jsonPath("$.price").value(PRODUCT_PRICE_SECOND))
                .andExpect(jsonPath("$.currency").value(CURRENCY_BASE))
                .andExpect(jsonPath("$.category.categoryId").value(CATEGORY_ID));
    }


    @Test
    @WithMockUser(authorities = "ADMIN")
    void deleteSuccess() throws Exception {
        // given
        doNothing().when(this.productsRepositoryMock).delete(eq(productsTest));
        when(this.productsRepositoryMock.findById(anyLong()))
                .thenReturn(Optional.ofNullable(productsTest));

        // when
        mockMvc.perform(delete("/products/{id}", PRODUCT_ID))
                .andExpect(status().isOk());
    }

    @Test
    @WithMockUser(authorities = "ADMIN")
    void deleteNotFoundProduct() throws Exception {
        //  given
        doNothing().when(this.productsRepositoryMock).delete(eq(productsTest));
        when(this.productsRepositoryMock.findById(anyLong()))
                .thenThrow(ProductsNotFoundException.class);
        // when
        mockMvc.perform(delete("/products/{id}", PRODUCT_ID))
                .andExpect(status().isNotFound());
    }
}