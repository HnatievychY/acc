package com.epam.acc.service;

import com.epam.acc.dto.request.ProductsRequest;
import com.epam.acc.dto.response.ProductsResponse;
import com.epam.acc.exeption.CategoriesNotFoundException;
import com.epam.acc.exeption.CurrencyNotSupportException;
import com.epam.acc.exeption.ProductsNotFoundException;
import com.epam.acc.mapper.ProductsMapper;
import com.epam.acc.model.Categories;
import com.epam.acc.model.LatestRate;
import com.epam.acc.model.Products;
import com.epam.acc.repository.ProductsRepository;
import com.epam.acc.service.impl.ProductsServiceImpl;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.MockitoAnnotations;

import java.math.BigDecimal;
import java.util.*;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.*;

class ProductsServiceTest {

    @InjectMocks
    private ProductsServiceImpl testObj;
    @Mock
    private ProductsRepository productsRepositoryMock;
    @Mock
    private ProductsMapper productMapperMock;
    @Mock
    private CategoriesService categoriesServiceMock;
    @Mock
    private FixerIoService fixerIoServiceMock;



    private List<ProductsResponse> productsResponseList;
    private List<ProductsResponse>productsResponseListByCategory;
    private List<Products>productsList;
    private List<Products>productsListByCategory;
    private Products firstProduct;
    private Products secondProduct;
    private ProductsResponse firstProductResponse;
    private ProductsResponse secondProductResponse;
    private ProductsRequest productsRequest;
    private ProductsRequest productsRequestCurrencyUSD;
    private Categories firstCategory;
    private Categories secondCategory;
    private LatestRate latestRate;

    private static final Long PRODUCT_ID_1 = 99L;
    private static final String PRODUCT_NAME_1 = "productName";
    private static final BigDecimal PRODUCT_PRICE_1 = new BigDecimal("10.12");
    private static final Long CATEGORY_ID_1 = 147L;
    private static final String CATEGORY_NAME_1 = "categoryName";

    private static final String CURRENCY_USD = "USD";
    private static final String CURRENCY_BASE ="EUR";
    private static final String CURRENCY_BAD ="UE";

    private static final Long PRODUCT_ID_2 = 145L;
    private static final String PRODUCT_NAME_2 = "secondProductName";
    private static final BigDecimal PRODUCT_PRICE_2 = new BigDecimal("20.21");
    private static final Long CATEGORY_ID_2 = 147L;
    private static final String CATEGORY_NAME_2 = "secondCategoryName";

    private Map<String, BigDecimal> rateMap;
    private static final BigDecimal PRODUCT_PRICE_USD_1 = new BigDecimal("12.14");
    private static final BigDecimal PRODUCT_PRICE_USD_2 = new BigDecimal("24.25");

    @BeforeEach
    public void setUp() {
        MockitoAnnotations.initMocks(this);

        rateMap = Map.of("USD", new BigDecimal(1.2));
        latestRate = new LatestRate();
        latestRate.setBase(CURRENCY_BASE);
        latestRate.setRates(rateMap);

        productsRequest = new ProductsRequest(PRODUCT_NAME_1,PRODUCT_PRICE_1,CURRENCY_BASE,CATEGORY_ID_1);
        productsRequestCurrencyUSD = new ProductsRequest(PRODUCT_NAME_1,PRODUCT_PRICE_1,CURRENCY_USD,CATEGORY_ID_1);

        firstCategory = new Categories();
        firstCategory.setCategoryId(CATEGORY_ID_1);
        firstCategory.setName(CATEGORY_NAME_1);

        secondCategory = new Categories();
        secondCategory.setCategoryId(CATEGORY_ID_2);
        secondCategory.setName(CATEGORY_NAME_2);

        firstProduct = new Products(PRODUCT_ID_1,PRODUCT_NAME_1,PRODUCT_PRICE_1, firstCategory);
        secondProduct = new Products(PRODUCT_ID_2,PRODUCT_NAME_2,PRODUCT_PRICE_2, secondCategory);
        firstProductResponse = new ProductsResponse(PRODUCT_ID_1,PRODUCT_NAME_1,PRODUCT_PRICE_1,CURRENCY_BASE, firstCategory);
        secondProductResponse = new ProductsResponse(PRODUCT_ID_2,PRODUCT_NAME_2,PRODUCT_PRICE_2,CURRENCY_USD, secondCategory);

        productsList = List.of(firstProduct,secondProduct);
        productsListByCategory = List.of(firstProduct);

        productsResponseListByCategory = List.of(firstProductResponse);

        productsResponseList = List.of(firstProductResponse,secondProductResponse);

    }

    @Test
    void getAllProducts() {
        // given
        when(this.productMapperMock.modelToResponseList(this.productsList))
                .thenReturn(this.productsResponseList);
        when(this.productsRepositoryMock.findAll())
                .thenReturn(this.productsList);

        // when
        List<ProductsResponse> productsResponseListActual = testObj.getAllProducts();
        // then
        assertNotNull(productsResponseListActual);
        assertEquals(2, productsResponseListActual.size());
        assertEquals(PRODUCT_NAME_1, productsResponseListActual.get(0).getName());
        assertEquals(firstCategory, productsResponseListActual.get(0).getCategory());
        assertEquals(CURRENCY_BASE, productsResponseListActual.get(0).getCurrency());
        assertEquals(PRODUCT_ID_1, productsResponseListActual.get(0).getProductId());
        assertEquals(PRODUCT_PRICE_1, productsResponseListActual.get(0).getPrice());
        assertEquals(PRODUCT_PRICE_2, productsResponseListActual.get(1).getPrice());
        assertEquals(PRODUCT_ID_2, productsResponseListActual.get(1).getProductId());
        assertEquals(PRODUCT_NAME_2, productsResponseListActual.get(1).getName());
        assertEquals(secondCategory, productsResponseListActual.get(1).getCategory());
        assertEquals(CURRENCY_USD, productsResponseListActual.get(1).getCurrency());


        verify(this.productMapperMock, times(1)).modelToResponseList(any(List.class));
        verify(this.productsRepositoryMock, times(1)).findAll();
    }

    @Test
    void getAllProductsByCurrency() {

        // given
        when(this.productMapperMock.modelToResponseList(this.productsList))
                .thenReturn(this.productsResponseList);
        when(this.productsRepositoryMock.findAll())
                .thenReturn(this.productsList);
        when(this.fixerIoServiceMock.latestRate(CURRENCY_USD))
                .thenReturn(this.latestRate);

        // when
        List<ProductsResponse> productsResponseListActual = testObj.getAllProductsByCurrency(CURRENCY_USD);
        // then
        assertNotNull(productsResponseListActual);
        assertEquals(2, productsResponseListActual.size());
        assertEquals(PRODUCT_NAME_1, productsResponseListActual.get(0).getName());
        assertEquals(firstCategory, productsResponseListActual.get(0).getCategory());
        assertEquals(CURRENCY_USD, productsResponseListActual.get(0).getCurrency());
        assertEquals(PRODUCT_ID_1, productsResponseListActual.get(0).getProductId());
        assertEquals(PRODUCT_PRICE_USD_1, productsResponseListActual.get(0).getPrice());
        assertEquals(PRODUCT_ID_1, productsResponseListActual.get(0).getProductId());
        assertEquals(PRODUCT_ID_2, productsResponseListActual.get(1).getProductId());
        assertEquals(PRODUCT_PRICE_USD_2, productsResponseListActual.get(1).getPrice());
        assertEquals(PRODUCT_NAME_2, productsResponseListActual.get(1).getName());
        assertEquals(secondCategory, productsResponseListActual.get(1).getCategory());
        assertEquals(CURRENCY_USD, productsResponseListActual.get(1).getCurrency());


        verify(this.productMapperMock, times(1)).modelToResponseList(any(List.class));
        verify(this.productsRepositoryMock, times(1)).findAll();
        verify(this.fixerIoServiceMock, times(1)).latestRate(CURRENCY_USD);
    }


    @Test
    void create() {
        //given
        when(this.productMapperMock.requestToModel(productsRequest))
                .thenReturn(this.firstProduct);
        when(this.productsRepositoryMock.save(firstProduct))
                .thenReturn(this.firstProduct);
        when(this.productMapperMock.modelToResponse(firstProduct))
                .thenReturn(this.firstProductResponse);
        when(this.categoriesServiceMock.getByIdReturnModel(CATEGORY_ID_1))
                .thenReturn(this.firstCategory);
        //when
        ProductsResponse productsResponseActual = testObj.create(productsRequest);
        //then
        assertNotNull(productsResponseActual);
        assertEquals(PRODUCT_ID_1, productsResponseActual.getProductId());
        assertEquals(PRODUCT_NAME_1, productsResponseActual.getName());
        assertEquals(PRODUCT_PRICE_1, productsResponseActual.getPrice());
        assertEquals(firstCategory, productsResponseActual.getCategory());
        assertEquals(CURRENCY_BASE, productsResponseActual.getCurrency());

        verify(this.productsRepositoryMock, times(1)).save(any(Products.class));
        verify(this.productMapperMock, times(1)).modelToResponse(any(Products.class));
        verify(this.productMapperMock, times(1)).requestToModel(any(ProductsRequest.class));
    }

    @Test
    void getAllProductsByCategoryId() {
        // given
        when(this.productMapperMock.modelToResponseList(this.productsListByCategory))
                .thenReturn(this.productsResponseListByCategory);
        when(this.productsRepositoryMock.findByCategory(CATEGORY_ID_1))
                .thenReturn(this.productsListByCategory);

        // when
        List<ProductsResponse> productsResponseListActual = testObj.getAllProductsByCategoryId(CATEGORY_ID_1);

        // then
        assertNotNull(productsResponseListActual);
        assertEquals(1, productsResponseListActual.size());
        assertEquals(PRODUCT_NAME_1, productsResponseListActual.get(0).getName());
        assertEquals(firstCategory, productsResponseListActual.get(0).getCategory());
        assertEquals(CURRENCY_BASE, productsResponseListActual.get(0).getCurrency());
        assertEquals(PRODUCT_ID_1, productsResponseListActual.get(0).getProductId());


        verify(this.productMapperMock, times(1)).modelToResponseList(any(List.class));
        verify(this.productsRepositoryMock, times(1)).findByCategory(CATEGORY_ID_1);
    }

    @Test
    void getByIdSuccess() {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));
        when(this.productMapperMock.modelToResponse(any(Products.class)))
                .thenReturn(firstProductResponse);
        // when
        ProductsResponse productsActual = testObj.getById(PRODUCT_ID_1);

        // then
        assertNotNull(productsActual);
        assertEquals(PRODUCT_ID_1, productsActual.getProductId());
        assertEquals(PRODUCT_NAME_1, productsActual.getName());
        assertEquals(PRODUCT_PRICE_1, productsActual.getPrice());
        assertEquals(firstCategory, productsActual.getCategory());

        verify(this.productsRepositoryMock, times(1)).findById(anyLong());
        verify(this.productMapperMock, times(1)).modelToResponse(any(Products.class));
    }

    @Test
    void getByIdException() {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenThrow(ProductsNotFoundException.class);
        when(this.productMapperMock.modelToResponse(any(Products.class)))
                .thenReturn(firstProductResponse);
        // then
        assertThrows(ProductsNotFoundException.class, () -> {
            testObj.getById(PRODUCT_ID_1);
        });
    }

    @Test
    void getByIdConverted() {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));
        when(this.productMapperMock.modelToResponse(any(Products.class)))
                .thenReturn(firstProductResponse);
        when(this.fixerIoServiceMock.latestRate(CURRENCY_USD))
                .thenReturn(latestRate);
        // when
        ProductsResponse productsActual = testObj.getByIdConverted(PRODUCT_ID_1,CURRENCY_USD);

        // then
        assertNotNull(productsActual);
        assertEquals(PRODUCT_ID_1, productsActual.getProductId());
        assertEquals(PRODUCT_NAME_1, productsActual.getName());
        assertEquals(PRODUCT_PRICE_USD_1, productsActual.getPrice());
        assertEquals(firstCategory, productsActual.getCategory());
        assertEquals(CURRENCY_USD, productsActual.getCurrency());

        verify(this.productsRepositoryMock, times(1)).findById(anyLong());
        verify(this.productMapperMock, times(1)).modelToResponse(any(Products.class));
    }

    @Test
    void getByIdConvertedCurrencyException() {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));
        when(this.productMapperMock.modelToResponse(any(Products.class)))
                .thenReturn(firstProductResponse);
        when(this.fixerIoServiceMock.latestRate(CURRENCY_BAD))
                .thenThrow(CurrencyNotSupportException.class);
        // then
        assertThrows(CurrencyNotSupportException.class, () -> {
            testObj.getByIdConverted(PRODUCT_ID_1, CURRENCY_BAD);
        });

    }

    @Test
    void getByIdReturnModelSuccess() {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));

        // when
        Products productsActual = testObj.getByIdReturnModel(PRODUCT_ID_1);

        // then
        assertNotNull(productsActual);
        assertEquals(PRODUCT_ID_1, productsActual.getProductId());
        assertEquals(PRODUCT_NAME_1, productsActual.getName());
        assertEquals(PRODUCT_PRICE_1, productsActual.getPrice());
        assertEquals(firstCategory, productsActual.getCategory());

        verify(this.productsRepositoryMock, times(1)).findById(anyLong());

    }

    @Test
    void getByIdReturnModelException() {
        // given
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenThrow(ProductsNotFoundException.class);

        // then
        assertThrows(ProductsNotFoundException.class, () -> {
            testObj.getByIdReturnModel(PRODUCT_ID_1);
        });
    }

    @Test
    void updateSuccessWithCurrencyEUR() {
        // given
        when(this.categoriesServiceMock.getByIdReturnModel(CATEGORY_ID_1))
                .thenReturn(this.firstCategory);
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));
        when(this.productsRepositoryMock.save(firstProduct))
                .thenReturn(this.firstProduct);
        when(this.productMapperMock.requestToModel(any(ProductsRequest.class),any(Products.class)))
                .thenReturn(firstProduct);
        when(this.productMapperMock.modelToResponse(firstProduct))
                .thenReturn(firstProductResponse);

        // when
        ProductsResponse productsResponseActual = testObj.update(productsRequest,PRODUCT_ID_1);

        // then
        assertNotNull(productsResponseActual);
        assertEquals(PRODUCT_ID_1, productsResponseActual.getProductId());
        assertEquals(PRODUCT_NAME_1, productsResponseActual.getName());
        assertEquals(PRODUCT_PRICE_1, productsResponseActual.getPrice());
        assertEquals(CURRENCY_BASE, productsResponseActual.getCurrency());
        assertEquals(firstCategory, productsResponseActual.getCategory());

        verify(this.productMapperMock, times(1)).modelToResponse(any(Products.class));
        verify(this.productMapperMock, times(1)).requestToModel(any(ProductsRequest.class),any(Products.class));
        verify(this.productsRepositoryMock, times(1)).findById(anyLong());
    }

    @Test
    void updateSuccessWithCurrencyUSD() {
        // given
        when(this.categoriesServiceMock.getByIdReturnModel(CATEGORY_ID_1))
                .thenReturn(this.firstCategory);
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));
        when(this.productsRepositoryMock.save(firstProduct))
                .thenReturn(this.firstProduct);
        when(this.productMapperMock.requestToModel(any(ProductsRequest.class),any(Products.class)))
                .thenReturn(firstProduct);
        when(this.productMapperMock.modelToResponse(firstProduct))
                .thenReturn(firstProductResponse);
        when(this.fixerIoServiceMock.latestRate(CURRENCY_USD))
                .thenReturn(this.latestRate);

        // when
        ProductsResponse productsResponseActual = testObj.update(productsRequestCurrencyUSD,PRODUCT_ID_1);

        // then
        assertNotNull(productsResponseActual);
        assertEquals(PRODUCT_ID_1, productsResponseActual.getProductId());
        assertEquals(PRODUCT_NAME_1, productsResponseActual.getName());
        assertEquals(PRODUCT_PRICE_1, productsResponseActual.getPrice());
        assertEquals(CURRENCY_BASE, productsResponseActual.getCurrency());
        assertEquals(firstCategory, productsResponseActual.getCategory());

        verify(this.productMapperMock, times(1)).modelToResponse(any(Products.class));
        verify(this.productMapperMock, times(1)).requestToModel(any(ProductsRequest.class),any(Products.class));
        verify(this.productsRepositoryMock, times(1)).findById(anyLong());
    }

    @Test()
    void updateSuccessWithCategoryException() {
        // given
        when(this.categoriesServiceMock.getByIdReturnModel(CATEGORY_ID_1))
                .thenThrow(CategoriesNotFoundException.class);
        when(this.productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(this.firstProduct));
        when(this.productsRepositoryMock.save(firstProduct))
                .thenReturn(this.firstProduct);
        when(this.productMapperMock.requestToModel(any(ProductsRequest.class),any(Products.class)))
                .thenReturn(firstProduct);
        when(this.productMapperMock.modelToResponse(firstProduct))
                .thenReturn(firstProductResponse);

        // then
        assertThrows(CategoriesNotFoundException.class, () -> {
            testObj.update(productsRequest,PRODUCT_ID_1);
        });
    }

    @Test
    void deleteSuccess() {
        // given
        doNothing().when(productsRepositoryMock).delete(Mockito.any(Products.class));
        when(productsRepositoryMock.findById(PRODUCT_ID_1))
                .thenReturn(Optional.ofNullable(firstProduct));
        // when
        testObj.delete(PRODUCT_ID_1);

        // then
        verify(this.productsRepositoryMock, times(1)).findById(any(Long.class));
        verify(this.productsRepositoryMock, times(1)).delete(Mockito.any(Products.class));
    }
}