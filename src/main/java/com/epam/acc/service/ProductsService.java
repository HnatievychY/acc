package com.epam.acc.service;

import com.epam.acc.dto.request.ProductsRequest;
import com.epam.acc.dto.response.ProductsResponse;
import com.epam.acc.model.Products;
import java.util.List;

public interface ProductsService {

    List<ProductsResponse> getAllProducts();
    List<ProductsResponse> getAllProductsByCurrency(String currencySymbol);
    ProductsResponse create (ProductsRequest productsRequest);
    List<ProductsResponse> getAllProductsByCategoryId(Long categoryId);
    ProductsResponse getById(Long productId);
    ProductsResponse getByIdConverted(Long productId,String currencySymbol);
    Products getByIdReturnModel(Long productId);
    ProductsResponse update(ProductsRequest productsRequest,Long productId);
    void delete (Long productId);
}
