package com.epam.acc.service;

import com.epam.acc.model.LatestRate;

public interface FixerIoService {
    LatestRate latestRate(String symbols);
}
