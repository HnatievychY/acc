package com.epam.acc.service;

import com.epam.acc.model.User;

public interface UserService {
    User findByUsername(String username);
}
