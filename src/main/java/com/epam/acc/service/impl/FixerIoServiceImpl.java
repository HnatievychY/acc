package com.epam.acc.service.impl;

import com.epam.acc.exeption.CurrencyNotSupportException;
import com.epam.acc.model.LatestRate;
import com.epam.acc.service.FixerIoService;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.util.UriComponentsBuilder;


@RequiredArgsConstructor
@Service
public class FixerIoServiceImpl implements FixerIoService {

    @Value("${fixer.API_ACCESS_KEY}")
    private String API_ACCESS_KEY;
    @Value("${fixer.BASE_CURRENCY}")
    private String BASE_CURRENCY;

    private final RestTemplate restTemplate;

    @Override
    public LatestRate latestRate(String symbols) {
        UriComponentsBuilder builder = UriComponentsBuilder.fromHttpUrl("http://data.fixer.io/api/latest")
                    .queryParam("access_key", API_ACCESS_KEY)
                    .queryParam("base", BASE_CURRENCY)
                    .queryParam("symbols", symbols);

        LatestRate latestRate = restTemplate.getForObject(builder.toUriString(), LatestRate.class);
        if(!latestRate.isSuccess()){
            throw  new CurrencyNotSupportException("not found currency symbol "+symbols);
        }
            return latestRate;
        }

    }
