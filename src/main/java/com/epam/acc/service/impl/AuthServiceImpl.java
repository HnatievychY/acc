package com.epam.acc.service.impl;

import com.epam.acc.dto.request.AuthRequest;
import com.epam.acc.dto.response.AuthResponse;
import com.epam.acc.security.JWTProvider;
import com.epam.acc.security.JwtUserDetailsService;
import com.epam.acc.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;


@Service
@RequiredArgsConstructor
@Slf4j
public class AuthServiceImpl implements AuthService {

    private final AuthenticationManager authenticationManager;
    private final JWTProvider jwtProvider;
    private final JwtUserDetailsService userDetailsService;


    @Override
    public AuthResponse authentication(AuthRequest authRequest) {
        UsernamePasswordAuthenticationToken token =
                new UsernamePasswordAuthenticationToken(authRequest.getUsername(), authRequest.getPassword());
        try {
            authenticationManager.authenticate(token);
        } catch (BadCredentialsException e) {
            log.error("Invalid credentials", e);
            throw e;
        }

        UserDetails userDetails = userDetailsService.loadUserByUsername(authRequest.getUsername());

        return new AuthResponse(jwtProvider.generateToken(userDetails));
    }
}

