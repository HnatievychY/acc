package com.epam.acc.service.impl;

import com.epam.acc.dto.request.CategoriesRequest;
import com.epam.acc.dto.response.CategoriesResponse;
import com.epam.acc.exeption.CategoriesAlreadyExistException;
import com.epam.acc.exeption.CategoriesNotFoundException;
import com.epam.acc.mapper.CategoriesMapper;
import com.epam.acc.model.Categories;
import com.epam.acc.repository.CategoriesRepository;
import com.epam.acc.service.CategoriesService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@RequiredArgsConstructor
public class CategoriesServiceImpl implements CategoriesService {

    private final CategoriesRepository categoriesRepository;
    private final CategoriesMapper categoriesMapper;

    @Override
    public List<CategoriesResponse> getAllCategories() {
        return categoriesMapper.modelToResponseList(categoriesRepository.findAll());
    }

    @Override
    public CategoriesResponse create(CategoriesRequest categoriesRequest) {
        if(existsCategoryByName(categoriesRequest.getName())){
            throw new CategoriesAlreadyExistException("A category with this name already exists");
        }
        Categories categories = categoriesMapper.requestToModel(categoriesRequest);
        return categoriesMapper.modelToResponse(categoriesRepository.save(categories));
    }


    @Override
    public CategoriesResponse getById(Long categoryId) {
        Categories categories = categoriesRepository.findById(categoryId)
                .orElseThrow(()-> new CategoriesNotFoundException("Category with id "+categoryId+" not found"));
        return categoriesMapper.modelToResponse(categories);
    }

    @Override
    public Categories getByIdReturnModel(Long categoryId) {
        return categoriesRepository.findById(categoryId)
                .orElseThrow(()-> new CategoriesNotFoundException("Category with id "+categoryId+" not found"));
    }

    @Override
    public CategoriesResponse update(CategoriesRequest categoriesRequest,Long categoryId) {
        Categories oldCategory = getByIdReturnModel(categoryId);
        Categories categories = categoriesMapper.requestToModel(categoriesRequest,oldCategory);
        return categoriesMapper.modelToResponse(categoriesRepository.save(categories));
    }

    @Override
    public Boolean existsCategoryByName (String name) {
        return categoriesRepository.existsCategoriesByName(name);
    }


    @Override
    public void delete(Long categoryId) {
        categoriesRepository.delete(getByIdReturnModel (categoryId));
    }
}
