package com.epam.acc.service.impl;

import com.epam.acc.dto.request.ProductsRequest;
import com.epam.acc.dto.response.ProductsResponse;
import com.epam.acc.exeption.ProductsNotFoundException;
import com.epam.acc.mapper.ProductsMapper;
import com.epam.acc.model.Categories;
import com.epam.acc.model.LatestRate;
import com.epam.acc.model.Products;
import com.epam.acc.repository.ProductsRepository;
import com.epam.acc.service.CategoriesService;
import com.epam.acc.service.FixerIoService;
import com.epam.acc.service.ProductsService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import java.math.RoundingMode;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProductsServiceImpl implements ProductsService {

    private final String BASE_CURRENCY = "EUR";
    private final ProductsRepository productsRepository;
    private final ProductsMapper productMapper;
    private final CategoriesService categoriesService;
    private final FixerIoService fixerIoService;

    @Override
    public List<ProductsResponse> getAllProducts() {
        return productMapper.modelToResponseList(productsRepository.findAll());
    }

    @Override
    public List<ProductsResponse> getAllProductsByCurrency(String currencySymbol) {
            LatestRate latestRate = fixerIoService.latestRate(currencySymbol);
            List<Products> productsList = productsRepository.findAll();
            List<ProductsResponse> productsResponseList = productMapper.modelToResponseList(productsList);
            productsResponseList.stream()
                    .peek(s->convertEURtoOtherCurrency(s,latestRate,currencySymbol))
                    .collect(Collectors.toList());
            return productsResponseList;
    }


    @Override
    public ProductsResponse create(ProductsRequest productsRequest) {
        if(!productsRequest.getCurrency().equals(BASE_CURRENCY)){
            updateProductPriceByCurrency(productsRequest);
        }
        Categories categories = categoriesService.getByIdReturnModel(productsRequest.getCategoryId());
        Products products = productMapper.requestToModel(productsRequest);
        products.setCategory(categories);
        return productMapper.modelToResponse(productsRepository.save(products));
    }

    @Override
    public List<ProductsResponse> getAllProductsByCategoryId(Long categoryId) {
        categoriesService.getByIdReturnModel(categoryId);
        List<Products> productsList = productsRepository.findByCategory(categoryId);
        return productMapper.modelToResponseList(productsList);
    }

    @Override
    public ProductsResponse getById(Long productId) {
        Products products = productsRepository.findById(productId)
                .orElseThrow(()-> new ProductsNotFoundException("Product with id "+productId+" not found"));
            return productMapper.modelToResponse(products);
        }

    @Override
    public ProductsResponse getByIdConverted(Long productId,String currencySymbol) {
        ProductsResponse productsResponse = getById(productId);
        LatestRate latestRate = fixerIoService.latestRate(currencySymbol);
        convertEURtoOtherCurrency(productsResponse, latestRate,currencySymbol);
        return productsResponse;
    }


    @Override
    public Products getByIdReturnModel(Long productId) {
        return productsRepository.findById(productId)
                .orElseThrow(()-> new ProductsNotFoundException("Product with id "+productId+" not found"));
    }

    @Override
    public ProductsResponse update(ProductsRequest productsRequest, Long productId) {
        Categories category = categoriesService.getByIdReturnModel(productsRequest.getCategoryId());
        if(!productsRequest.getCurrency().equals(BASE_CURRENCY)){
            updateProductPriceByCurrency(productsRequest);
        }
        Products oldProduct = getByIdReturnModel(productId);
        Products products = productMapper.requestToModel(productsRequest,oldProduct);
        products.setCategory(category);
        return productMapper.modelToResponse(productsRepository.save(products));
    }

    @Override
    public void delete(Long productId) {
        Products products = getByIdReturnModel(productId);
        productsRepository.delete(products);
    }

    public ProductsRequest updateProductPriceByCurrency(ProductsRequest productsRequest) {
        LatestRate latestRate = fixerIoService.latestRate(productsRequest.getCurrency());
        convertOtherCurrencyToEUR(productsRequest,latestRate);
        return productsRequest;
    }

    private void convertEURtoOtherCurrency(
            ProductsResponse productsResponse, LatestRate latestRate, String symbol){
        productsResponse.setPrice(productsResponse.getPrice().multiply(latestRate.getRates().get(symbol.toUpperCase()))
                .setScale(2, RoundingMode.HALF_UP));
        productsResponse.setCurrency(symbol);
    }

    private void convertOtherCurrencyToEUR(
            ProductsRequest productsRequest, LatestRate latestRate){
        productsRequest.setPrice(productsRequest.getPrice()
                .divide(latestRate.getRates().get(productsRequest.getCurrency().toUpperCase()),2, RoundingMode.HALF_UP));
    }

}
