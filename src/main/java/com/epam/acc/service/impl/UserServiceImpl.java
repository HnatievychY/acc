package com.epam.acc.service.impl;

import com.epam.acc.exeption.UserNotFoundException;
import com.epam.acc.model.User;
import com.epam.acc.repository.UserRepository;
import com.epam.acc.service.UserService;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;

    @Override
    public User findByUsername(String username) {
        return userRepository.findByUsername(username)
                .orElseThrow(()->new UserNotFoundException("User not found with username: " + username));
    }
}
