package com.epam.acc.service;

import com.epam.acc.dto.request.CategoriesRequest;
import com.epam.acc.dto.response.CategoriesResponse;
import com.epam.acc.model.Categories;

import java.util.List;

public interface CategoriesService {

    List<CategoriesResponse> getAllCategories();
    CategoriesResponse create (CategoriesRequest categories);
    CategoriesResponse getById(Long categoryId);
    Categories getByIdReturnModel(Long categoryId);
    CategoriesResponse update(CategoriesRequest categories,Long categoryId);
    Boolean existsCategoryByName (String name);
    void delete (Long categoryId);

}
