package com.epam.acc.service;

import com.epam.acc.dto.request.AuthRequest;
import com.epam.acc.dto.response.AuthResponse;

public interface AuthService {
    AuthResponse authentication (AuthRequest authRequest);
}
