package com.epam.acc.controller;

import com.epam.acc.dto.request.AuthRequest;
import com.epam.acc.dto.response.AuthResponse;
import com.epam.acc.service.AuthService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.*;

@RestController
@Slf4j
@RequiredArgsConstructor
public class JwtAuthenticationController {

    private final AuthService authService;

    @RequestMapping(value = "/authenticate", method = RequestMethod.POST)
    @ResponseStatus(HttpStatus.OK)
    public AuthResponse createAuthenticationToken(@RequestBody AuthRequest authRequest) {
        return authService.authentication(authRequest);
    }
}
