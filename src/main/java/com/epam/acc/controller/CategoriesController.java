package com.epam.acc.controller;

import com.epam.acc.dto.request.CategoriesRequest;
import com.epam.acc.dto.response.CategoriesResponse;
import com.epam.acc.service.CategoriesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import java.util.List;


@RequiredArgsConstructor
@RestController
@RequestMapping("/categories")
public class CategoriesController {

    private final CategoriesService categoriesService;


    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public List<CategoriesResponse> findCategoryById() {
        return categoriesService.getAllCategories();
    }

    @RequestMapping(
            value = "{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(HttpStatus.OK)
    public CategoriesResponse getCategoriesById (@PathVariable("id") @Min(1) Long categoryId) {
        return categoriesService.getById(categoryId);
    }


    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public CategoriesResponse create(@RequestBody @Valid CategoriesRequest category) {
        return categoriesService.create(category);
    }


    @RequestMapping(method = RequestMethod.PUT,
            value = "{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public CategoriesResponse update (  @PathVariable("id") @Min(1) Long categoryId,
                                        @RequestBody @Valid CategoriesRequest category) {
        return categoriesService.update(category,categoryId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(
            value = "{id}",
            method = RequestMethod.DELETE)
    @ResponseStatus(HttpStatus.OK)
    public void delete(@PathVariable("id") @Min(1) Long categoryId) {
        categoriesService.delete(categoryId);
    }

}
