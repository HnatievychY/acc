package com.epam.acc.controller;

import com.epam.acc.dto.request.ProductsRequest;
import com.epam.acc.dto.response.ProductsResponse;
import com.epam.acc.service.ProductsService;
import com.epam.acc.validation.CurrencyValidation;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import javax.validation.constraints.Min;
import java.util.List;

@RequiredArgsConstructor
@RestController
@Validated
@RequestMapping("/products")
public class ProductsController {

    private final ProductsService productsService;

    @RequestMapping(
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductsResponse> getAllProducts() {
        return productsService.getAllProducts();
    }

    @RequestMapping(
            value = "currency/{currencySymbol}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductsResponse> getAllProducts( @PathVariable("currencySymbol") @CurrencyValidation String currencySymbol) {
        return productsService.getAllProductsByCurrency(currencySymbol);
    }


    @RequestMapping(
            value = "{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductsResponse getProductById(@PathVariable("id") @Min(1) Long productId) {
        return productsService.getById(productId);
    }

    @RequestMapping(
            value = "{id}/{currencySymbol}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public ProductsResponse getProductByIdOtherCurrency(@PathVariable("id") @Min(1) Long productId,
                                                        @PathVariable("currencySymbol") @CurrencyValidation String currencySymbol) {
        return productsService.getByIdConverted(productId,currencySymbol);
    }


    @RequestMapping(
            value = "category/{id}",
            method = RequestMethod.GET,
            produces = MediaType.APPLICATION_JSON_VALUE)
    public List<ProductsResponse> findProductByCategory(@PathVariable("id")@Min(1) Long categoryId) {
        return productsService.getAllProductsByCategoryId(categoryId);
    }

    @RequestMapping(method = RequestMethod.POST,
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.CREATED)
    public ProductsResponse create(@RequestBody @Validated ProductsRequest productsRequest) {
        return productsService.create(productsRequest);
    }



    @RequestMapping(method = RequestMethod.PUT,
            value = "{id}",
            consumes = MediaType.APPLICATION_JSON_VALUE,
            produces = MediaType.APPLICATION_JSON_VALUE)
    @ResponseStatus(value = HttpStatus.OK)
    public ProductsResponse update (
            @PathVariable("id") @Min(1) Long productId,
            @RequestBody @Validated ProductsRequest productsRequest) {
        return productsService.update(productsRequest, productId);
    }

    @PreAuthorize("hasAuthority('ADMIN')")
    @RequestMapping(method = RequestMethod.DELETE,
                    value = "{id}")
    @ResponseStatus(value = HttpStatus.OK)
    public void delete(@PathVariable("id") @Min(1) Long productId) {
        productsService.delete(productId);
    }
}
