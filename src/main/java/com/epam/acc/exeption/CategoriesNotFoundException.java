package com.epam.acc.exeption;

public class CategoriesNotFoundException extends RuntimeException {
    static final long serialVersionUID = -6879914928840050210L;

    public CategoriesNotFoundException(String message) {
        super(message);
    }
}
