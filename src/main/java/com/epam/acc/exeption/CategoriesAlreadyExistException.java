package com.epam.acc.exeption;

public class CategoriesAlreadyExistException extends RuntimeException {

    static final long serialVersionUID = -687991492884005033L;

    public CategoriesAlreadyExistException(String message) {
        super(message);
    }

}
