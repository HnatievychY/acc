package com.epam.acc.exeption;

public class CurrencyNotSupportException extends RuntimeException {
    static final long serialVersionUID = -687789492994005038L;

    public CurrencyNotSupportException(String message) {
        super(message);
    }
}

