package com.epam.acc.exeption;

public class UserNotFoundException extends RuntimeException {

    static final long serialVersionUID = -687789492886545034L;

    public UserNotFoundException (String message) {
        super(message);
    }
}
