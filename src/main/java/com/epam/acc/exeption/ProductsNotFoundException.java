package com.epam.acc.exeption;

public class ProductsNotFoundException extends RuntimeException {

    static final long serialVersionUID = -687789492884005034L;

    public ProductsNotFoundException(String message) {
        super(message);
    }
}
