package com.epam.acc.dto.request;

import com.epam.acc.validation.CurrencyValidation;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.validation.constraints.DecimalMin;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import java.math.BigDecimal;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductsRequest {

    @NotEmpty
    private String name;
    @DecimalMin("0.01")
    private BigDecimal price;
    @CurrencyValidation
    private String currency;
    @Min(1)
    private Long categoryId;
}
