package com.epam.acc.dto.response;

import com.epam.acc.model.Categories;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.math.BigDecimal;



@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
public class ProductsResponse {

    private long productId;
    private String name;
    private BigDecimal price;
    private String currency = "EUR";
    private Categories category;
}
