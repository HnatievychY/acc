package com.epam.acc.validation;

import com.epam.acc.validation.validator.CurrencyConstraintValidator;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

@Target({ElementType.METHOD, ElementType.FIELD, ElementType.PARAMETER})
@Retention(RetentionPolicy.RUNTIME)
@Constraint(validatedBy = CurrencyConstraintValidator.class)

public @interface CurrencyValidation {

    String message() default "invalid currency symbol (like USD, EUR, UAH)";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
