package com.epam.acc.validation.validator;

import com.epam.acc.validation.CurrencyValidation;
import lombok.extern.slf4j.Slf4j;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@Slf4j
public class CurrencyConstraintValidator implements ConstraintValidator<CurrencyValidation, String> {

    private static final String CURRENCY_SYMBOL_REGEX = "^[a-zA-Z]{3}+$";

    private static final Pattern CURRENCY_SYMBOL_PATTERN = Pattern.compile(CURRENCY_SYMBOL_REGEX);

    @Override
    public boolean isValid(String currencySymbol, ConstraintValidatorContext constraintValidatorContext) {
        log.info("Validation of currency symbol value: {}", currencySymbol);
        Matcher matcher = CURRENCY_SYMBOL_PATTERN.matcher(currencySymbol);
        return matcher.matches();
    }
}
