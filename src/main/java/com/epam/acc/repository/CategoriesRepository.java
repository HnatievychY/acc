package com.epam.acc.repository;

import com.epam.acc.model.Categories;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoriesRepository extends JpaRepository<Categories,Long> {
    Boolean existsCategoriesByName (String name);
}
