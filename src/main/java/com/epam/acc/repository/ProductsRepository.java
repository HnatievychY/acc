package com.epam.acc.repository;

import com.epam.acc.model.Products;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductsRepository extends JpaRepository<Products,Long> {

    @Query(value = "SELECT * FROM products p where category_id = :id", nativeQuery = true)
    List<Products> findByCategory (@Param("id") Long id);
}
