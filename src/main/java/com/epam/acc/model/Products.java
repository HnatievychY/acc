package com.epam.acc.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.Objects;


@NoArgsConstructor
@AllArgsConstructor
@Setter
@Getter
@Entity
@Table(name = "products")
public class Products {

    @Id
    @Column(name = "product_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long productId;

    private String name;

    private BigDecimal price;
    @ManyToOne(cascade = CascadeType.DETACH)
    @JoinColumn(name = "category_id")
    private Categories category;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Products)) {
            return false;
        }
        Products products = (Products) o;
        return getProductId() == products.getProductId()
                && Objects.equals(getName(), products.getName())
                && Objects.equals(getPrice(), products.getPrice())
                && Objects.equals(getCategory(), products.getCategory());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getProductId(), getName(), getPrice(), getCategory());
    }

    @Override
    public String toString() {
        return "Products{" +
                "productId=" + productId +
                ", name='" + name + '\'' +
                ", price=" + price +
                ", category=" + category +
                '}';
    }
}
