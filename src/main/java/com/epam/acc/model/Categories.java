package com.epam.acc.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@NoArgsConstructor
@AllArgsConstructor
@Getter
@Setter
@Entity
@Table(name = "categories")
public class Categories {

    @Id
    @Column(name = "category_id")
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long categoryId;

    private String name;
    @OneToMany(mappedBy = "category")
    @JsonIgnore
    private List<Products> productsList = new ArrayList<>();

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (!(o instanceof Categories)) {
            return false;
        }
        Categories that = (Categories) o;
        return getCategoryId() == that.getCategoryId()
                && Objects.equals(getName(), that.getName())
                && Objects.equals(getProductsList(), that.getProductsList());
    }

    @Override
    public int hashCode() {
        return Objects.hash(getCategoryId(), getName(), getProductsList());
    }

    @Override
    public String toString() {
        return "Categories{" +
                "categoryId=" + categoryId +
                ", name='" + name + '\'' +
                '}';
    }
}
