package com.epam.acc.mapper;


import com.epam.acc.dto.request.ProductsRequest;
import com.epam.acc.dto.response.ProductsResponse;
import com.epam.acc.model.Products;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface ProductsMapper {

    ProductsResponse modelToResponse (Products products);

    Products requestToModel (ProductsRequest productsRequest);

    Products requestToModel(ProductsRequest productsRequest, @MappingTarget Products products);

    List<ProductsResponse> modelToResponseList(List<Products> products);
}
