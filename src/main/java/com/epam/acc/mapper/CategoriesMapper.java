package com.epam.acc.mapper;

import com.epam.acc.dto.request.CategoriesRequest;
import com.epam.acc.dto.response.CategoriesResponse;
import com.epam.acc.model.Categories;
import org.mapstruct.Mapper;
import org.mapstruct.MappingTarget;
import org.mapstruct.ReportingPolicy;

import java.util.List;

@Mapper(componentModel = "spring",unmappedTargetPolicy = ReportingPolicy.IGNORE)
public interface CategoriesMapper {

   CategoriesResponse modelToResponse (Categories categories);

   Categories requestToModel (CategoriesRequest categoriesRequest);

   Categories requestToModel(CategoriesRequest categoriesRequest, @MappingTarget Categories categories);

   List<CategoriesResponse> modelToResponseList(List<Categories> categories);
}
