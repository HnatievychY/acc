create table if not exists users (
  user_id int8 not null,
  username varchar(255) not null,
  password varchar(255) not null,
  role varchar(255) not null,
  primary key (user_id)
);

create table if not exists categories (
  category_id int8,
  name varchar(255) not null,
  PRIMARY KEY(category_id)
);

create table if not exists products (
  product_id int8,
  name varchar(255) not null,
  price NUMERIC not null,
  category_id int8 not null,
  PRIMARY KEY(product_id),
  CONSTRAINT fk_products
      FOREIGN KEY(category_id)
	  REFERENCES categories
)


